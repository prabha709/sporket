import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sporket/pages/contactus.dart';

import 'package:sporket/pages/forgotpass.dart';
import 'package:sporket/pages/login.dart';
import 'package:sporket/pages/privacypolicy.dart';
import 'package:sporket/pages/profileedit.dart';
import 'package:sporket/pages/security.dart';
import 'package:sporket/pages/signup.dart';
import 'package:sporket/pages/home.dart';
import 'package:sporket/pages/termscondition.dart';
import 'package:sporket/provider/google_sign_in.dart';
import 'pages/wallet.dart';


void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => GoogleSignInProvider(),
         
        ),
        
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        initialRoute: ('/'), 
        theme: ThemeData(
          //  primarySwatch: ColorThemes.primary,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: Login(),
        routes: {
          '/ ': (context) => Login(),
          '/signup': (context) => Signup(),
          '/forgotpass': (context) => Forgotpass(),
          '/home': (context) => Home(),
          '/privacypolicy': (context) => PrivacyPolicy(),
          '/termscondition': (context) => TermsnCondition(),
          '/wallet': (context) => Wallet(),
         

          '/contactus': (context) => ContactUs(),
          '/security': (context) => Security(),
          '/profileedit': (context) => EditUserProfile(),
          
        },
      ),
    );
  }
}
