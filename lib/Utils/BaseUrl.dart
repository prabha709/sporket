
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sporket/Utils/AppColors.dart';

class BaseUrl {
  static const url = "http://135.181.194.2/";

   static const imgurl = "http://ec2-3-108-252-72.ap-south-1.compute.amazonaws.com:8000";
}

class SucessMessage {
  final String? message;

  SucessMessage({
    this.message
  });
  suceessMessage() {
    return Fluttertoast.showToast(
        msg: message!,
        backgroundColor: AppColors.colorGreen);
  }
}

class FailureMessage {
  final String? message;

  FailureMessage({
    this.message
  });
  failedMessage() {
    return Fluttertoast.showToast(
        msg: message!, backgroundColor: AppColors.colorRed);
  }
}

class CustomLoader {
  loadingIcon(){
    return CircularProgressIndicator();
  }
}
class GifLoader {
  gifLoader(){
    return Image.asset("assets/loading.gif",width: 100.0,);
  }
}