import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sporket/pages/contactus.dart';
import 'package:sporket/pages/privacypolicy.dart';
import 'package:sporket/pages/profileedit.dart';
import 'package:sporket/pages/security.dart';
import 'package:sporket/pages/settings.dart';
import 'package:sporket/pages/termscondition.dart';
import 'package:sporket/pages/wallet.dart';
import 'package:sporket/tabs/contest_page.dart';

class NavDrawer extends StatefulWidget {
  @override
  _NavDrawerState createState() => _NavDrawerState();
}

class _NavDrawerState extends State<NavDrawer> {
  String? username, phone, dp;
  getUserDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      username = prefs.getString("username");
      phone = prefs.getString('phone');
      dp = prefs.getString("dp");
    });
  }

  @override
  void initState() {
    getUserDetails();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              color: HexColor("#C11F23"),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Material(
                  borderRadius: BorderRadius.all(Radius.circular(90.0)),
                  elevation: 2.0,
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.122,
                    width: MediaQuery.of(context).size.width * 0.250,
                    child: CircleAvatar(
                      radius: 40.0,
                      backgroundImage: NetworkImage(
                          "https://www.clipartmax.com/png/middle/405-4050774_avatar-icon-flat-icon-shop-download-free-icons-for-avatar-icon-flat.png"),
                    ),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(username!,
                        style: TextStyle(
                            color: Colors.white, fontFamily: 'Montserrat')),
                    SizedBox(height: 5),
                    Text(phone!,
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          color: Colors.white,
                        )),
                  ],
                ),
                InkWell(
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => EditUserProfile()));
                    },
                    child: Icon(Icons.edit, color: Colors.white)),
              ],
            ),
          ),
          ListTile(
            leading: Icon(Icons.person),
            title:
                Text('User Info', style: TextStyle(fontFamily: 'Montserrat')),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => EditUserProfile()));
            },
          ),
          ListTile(
            leading: Icon(Icons.account_balance_wallet),
            title:
                Text('My Wallet', style: TextStyle(fontFamily: 'Montserrat')),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Wallet()));
            },
          ),
          ListTile(
            leading: Icon(Icons.verified),
            title: Text(
              'My Contest',
              style: TextStyle(fontFamily: 'Montserrat'),
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ContestPage(
                    appbar: true,
                  ),
                ),
              );
            },
          ),
          ListTile(
            leading: Icon(Icons.mail),
            title:
                Text('Contact Us', style: TextStyle(fontFamily: 'Montserrat')),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ContactUs()));
            },
          ),
          ListTile(
            leading: Icon(
              Icons.file_copy,
            ),
            title: Text(
              'Terms and Conditions',
              style: TextStyle(fontFamily: 'Montserrat'),
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => TermsnCondition()));
            },
          ),
          ListTile(
            leading: Icon(Icons.accessibility),
            title: Text('Privacy Policy',
                style: TextStyle(fontFamily: 'Montserrat')),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => PrivacyPolicy()));
            },
          ),
          ListTile(
            leading: Icon(Icons.lock),
            title: Text('Security', style: TextStyle(fontFamily: 'Montserrat')),
            onTap: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Security()));
            },
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.06,
            width: MediaQuery.of(context).size.width * 0.04,
            margin: EdgeInsets.all(40),
            child: ElevatedButton(
                onPressed: () {},
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(HexColor("#0A37E0")),
                    padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                        EdgeInsets.all(10.0)),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            side: BorderSide(color: HexColor("#0A37E0"))))),
                child: Text(
                  "LOGOUT",
                  style: TextStyle(fontFamily: 'Montserrat'),
                )),
          )
        ],
      ),
    );
  }
}
