import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class EditUserProfile extends StatelessWidget {
  const EditUserProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width * 1000,
              height: MediaQuery.of(context).size.height * 0.245,
              color: HexColor("#C11F23"),
              padding: EdgeInsets.all(30),
              child: Stack(
                children: [
                  Center(
                    child: CircleAvatar(
                      radius: 50,
                      backgroundImage: AssetImage("assets/johncena.jpg"),
                    ),
                  ),
                  Positioned(
                      bottom: 10,
                      right: 50,
                      child: RawMaterialButton(
                        onPressed: () {},
                        elevation: 2.0,
                        fillColor: Color(0xFFF5F6F9),
                        child: Icon(
                          Icons.edit,
                          color: Colors.blue,
                        ),
                        padding: EdgeInsets.all(8.0),
                        shape: CircleBorder(),
                      )),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.713,
              width: MediaQuery.of(context).size.width * 350,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 15,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 40, right: 40),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "First Name *",
                          style:
                              TextStyle(fontFamily: 'Montserrat', fontSize: 16),
                        ),
                        TextField(),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Last Name *",
                          style:
                              TextStyle(fontFamily: 'Montserrat', fontSize: 16),
                        ),
                        TextField(),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Mobile No *",
                          style:
                              TextStyle(fontFamily: 'Montserrat', fontSize: 16),
                        ),
                        TextField(),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Email Address *",
                          style:
                              TextStyle(fontFamily: 'Montserrat', fontSize: 16),
                        ),
                        TextField(),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Password",
                          style:
                              TextStyle(fontFamily: 'Montserrat', fontSize: 16),
                        ),
                        TextField(),
                        TextButton(
                          onPressed: () {},
                          child: Text(
                            "Change Password",
                            style: TextStyle(
                              color: HexColor("#C11F23"),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.090,
                    width: MediaQuery.of(context).size.width * 1000,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ElevatedButton(
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  HexColor("#0A37E0")),
                              padding:
                                  MaterialStateProperty.all<EdgeInsetsGeometry>(
                                      EdgeInsets.all(15.0)),
                              shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5.0),
                                      side: BorderSide(
                                          color: HexColor("#0A37E0"))))),
                          onPressed: () {},
                          child: Text(
                            "Submit",
                            style: TextStyle(
                                fontSize: 16, fontFamily: 'Montserrat'),
                          )),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      )),
    );
  }
}
