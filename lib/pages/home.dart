import 'package:flutter/material.dart';
import 'package:sporket/pages/profileedit.dart';
import 'package:sporket/pages/wallet.dart';

import 'package:sporket/tabs/games_page.dart';
import 'package:sporket/tabs/home_page.dart';
import 'package:sporket/tabs/contest_page.dart';
//import 'package:sporket/tabs/profile_page.dart';
import 'package:sporket/tabs/leaderboard_page.dart';
import 'package:sporket/widgtes/nav_drawer.dart';

import 'notification_page.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final List<Widget> _pagesList = [
    HomePage(),
    GamesPage(),
    LeaderBoardPage(),
    ContestPage(),
    EditUserProfile()
  ];
  int _currentIndex = 0;

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          foregroundColor: Colors.black,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Wallet()));
                  },
                  child: Icon(
                    Icons.account_balance_wallet,
                    color: Colors.black,
                    size: 26.0,
                  ),
                )),
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => NotificationPage()));
                  },
                  child: Icon(
                    Icons.notifications,
                    color: Colors.black,
                  ),
                )),
          ]),
      body: _pagesList[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        selectedFontSize: 9.0,
        unselectedFontSize: 9.0,
        type: BottomNavigationBarType.fixed,
        showUnselectedLabels: true,
        unselectedItemColor: Colors.grey,
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            activeIcon: Padding(
              padding: const EdgeInsets.only(bottom: 2.5),
              child: SizedBox(
                height: 25,
                width: 25,
                child: Image.asset(
                  "assets/home.png",
                  //color: Colors.blue,
                ),
              ),
            ),
            label: 'Home',
            icon: Padding(
              padding: const EdgeInsets.only(bottom: 2.5),
              child: SizedBox(
                height: 25,
                width: 25,
                child: Image.asset(
                  "assets/home.png",
                  color: Colors.grey,
                ),
              ),
            ),
          ),
          BottomNavigationBarItem(
            label: 'Games',
            activeIcon: Padding(
              padding: const EdgeInsets.only(bottom: 2.5),
              child: SizedBox(
                height: 25,
                width: 25,
                child: Image.asset(
                  "assets/games.png",
                  //color: Colors.blue,
                ),
              ),
            ),
            icon: Padding(
              padding: const EdgeInsets.only(bottom: 2.5),
              child: SizedBox(
                height: 25,
                width: 25,
                child: Image.asset(
                  "assets/games.png",
                  color: Colors.grey,
                ),
              ),
            ),
          ),
          BottomNavigationBarItem(
            activeIcon: Padding(
              padding: const EdgeInsets.only(bottom: 2.5),
              child: SizedBox(
                height: 25,
                width: 25,
                child: Image.asset(
                  "assets/page.png",
                ),
              ),
            ),
            icon: Padding(
              padding: const EdgeInsets.only(bottom: 2.5),
              child: SizedBox(
                height: 25,
                width: 25,
                child: Image.asset(
                  "assets/page.png",
                  color: Colors.grey,
                ),
              ),
            ),
            label: 'Result',
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: const EdgeInsets.only(bottom: 2.5),
              child: SizedBox(
                height: 25,
                width: 25,
                child: Image.asset(
                  "assets/contest.png",
                  color: Colors.grey,
                ),
              ),
            ),
            activeIcon: Padding(
              padding: const EdgeInsets.only(bottom: 2.5),
              child: SizedBox(
                height: 25,
                width: 25,
                child: Image.asset(
                  "assets/contest.png",
                ),
              ),
            ),
            label: 'Contest',
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: const EdgeInsets.only(bottom: 2.5),
              child: SizedBox(
                height: 25,
                width: 25,
                child: Icon(
                  Icons.person,
                  color: Colors.grey,
                ),
              ),
            ),
            activeIcon: Padding(
              padding: const EdgeInsets.only(bottom: 2.5),
              child: SizedBox(
                height: 25,
                width: 25,
                child: Icon(
                  Icons.person,
                  color: Colors.blue,
                ),
              ),
            ),
            label: 'Profile',
          ),
        ],
      ),
    );
  }
}
