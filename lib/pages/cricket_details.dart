import 'package:flutter/material.dart';
import 'package:sporket/pages/contest_page_details.dart';
import 'package:intl/intl.dart';

class CricketDetailsPage extends StatefulWidget {
  final String? seriesName;
  final List? upcomingSeries;
  CricketDetailsPage({this.seriesName, this.upcomingSeries});

  @override
  _CricketDetailsPageState createState() => _CricketDetailsPageState();
}

class _CricketDetailsPageState extends State<CricketDetailsPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          foregroundColor: Colors.black,
          leading: BackButton(color: Colors.black),
          actions: <Widget>[
            // Padding(
            //     padding: EdgeInsets.only(right: 20.0),
            //     child: GestureDetector(
            //       onTap: () {},
            //       child: Icon(
            //         Icons.account_balance_wallet,
            //         color: Colors.black,
            //         size: 26.0,
            //       ),
            //     )),
            // Padding(
            //     padding: EdgeInsets.only(right: 20.0),
            //     child: GestureDetector(
            //       onTap: () {},
            //       child: Icon(
            //         Icons.notifications,
            //         color: Colors.black,
            //       ),
            //     )),
          ]),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            Container(
              height: MediaQuery.of(context).size.height / 3.5,
              width: MediaQuery.of(context).size.width,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
                clipBehavior: Clip.antiAlias,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: <Widget>[
                          Container(
                            height: MediaQuery.of(context).size.height * 0.120,
                            width: MediaQuery.of(context).size.height * 0.10,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              image: DecorationImage(
                                image: AssetImage('assets/csklion.jpg'),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                // SizedBox(
                                //   height: 30,
                                // ),
                                Text(
                                  widget.seriesName!,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18.0),
                                ),
                                SizedBox(
                                  height: 35,
                                ),
                                Text(
                                  "Regular Championship ",
                                  style: TextStyle(fontSize: 10),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      const Divider(
                        color: Colors.black,
                      ),
                      Row(
                        //mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Column(
                            //crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Popularity"),
                              //Text("Popularity"),
                              Card(
                                elevation: 5.0,
                                child: SizedBox(
                                  width: MediaQuery.of(context).size.width / 5,
                                  child: LinearProgressIndicator(
                                      value: 0.47, backgroundColor: Colors.white
                                      //valueColor: changeColor),
                                      ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            width: 150,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text("Overall rating"),
                              //Text("Popularity"),
                              SizedBox(
                                width: MediaQuery.of(context).size.width / 5,
                                child: LinearProgressIndicator(
                                    value: 0.7,
                                    color: Colors.orange,
                                    backgroundColor: Colors.white
                                    //valueColor: changeColor),
                                    ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            ListView.builder(
              shrinkWrap: true,
              itemCount: widget.upcomingSeries!.length,
              itemBuilder: (context, index) {
                // DateTime startDate =
                //     DateTime.parse(widget.upcomingSeries![index]["start_time"]);
                // DateTime endDate =
                //     DateTime.parse(widget.upcomingSeries![index]["end_time"]);
                // String dateString =
                //     DateFormat('dd MMM, yyyy').format(startDate);
                // String dateEnd = DateFormat('dd MMM, yyyy').format(endDate);

                return Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ContestPageDetais(
                                    startDate: widget.upcomingSeries![index]
                                        ["end_time"],
                                    startTime: widget.upcomingSeries![index]
                                        ["start_time"],
                                    team1: widget.upcomingSeries![index]
                                        ["team1"],
                                    team2: widget.upcomingSeries![index]
                                        ["team2"],
                                    priceDetails: widget.upcomingSeries![index]
                                        ["prizepools"],
                                        balTime: widget.upcomingSeries![index]
                                        ["balance_time"],
                                  )));
                    },
                    child: Card(
                      elevation: 5.0,
                      child: Container(
                        height: MediaQuery.of(context).size.height / 6,
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 18.0, left: 18.0),
                                  child: Column(
                                    children: [
                                      Container(
                                        height:
                                            MediaQuery.of(context).size.height *
                                                0.08,
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.15,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5)),
                                          image: DecorationImage(
                                            image: AssetImage('assets/mcc.png'),
                                            fit: BoxFit.fill,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5.0,
                                      ),
                                      Text(
                                        widget.upcomingSeries![index]["team1"],
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 15),
                                      ),
                                      Text(
                                        "Start Date "+ widget.upcomingSeries![index]
                                        ["start_time"] ,
                                        style: TextStyle(

                                            //fontWeight: FontWeight.bold,
                                            fontSize: 10),
                                      ),
                                    ],
                                  ),
                                ),
                                Text(
                                  "Vs",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 25),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 18.0, right: 18.0),
                                  child: Column(
                                    children: [
                                      Container(
                                        height:
                                            MediaQuery.of(context).size.height *
                                                0.08,
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.15,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5)),
                                          image: DecorationImage(
                                            image:
                                                AssetImage('assets/mufc.png'),
                                            fit: BoxFit.fill,
                                          ),
                                        ),
                                      ),
                                      Text(
                                        widget.upcomingSeries![index]["team2"],
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 15),
                                      ),
                                      Text(
                                        "End Date " +widget.upcomingSeries![index]
                                        ["end_time"],
                                        style: TextStyle(

                                            //fontWeight: FontWeight.bold,
                                            fontSize: 10),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
