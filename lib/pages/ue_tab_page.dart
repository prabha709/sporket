import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:flutter_countdown_timer/index.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sporket/pages/Questions.dart';
import 'package:intl/intl.dart';

class UETabPage extends StatefulWidget {
  final String? startTime;
  final String? startDate;
  final String? team1;
  final String? team2;
  final List? priceDetails;
  final int ? balTime;


  UETabPage({
    this.team2,
    this.team1,
    this.startTime,
    this.startDate,
    this.priceDetails,
    this.balTime
   
  });

  @override
  _UETabPageState createState() => _UETabPageState();
}

class _UETabPageState extends State<UETabPage> {
int ?countertime =0;
bool isEnded = false;
CountdownTimerController ?controller;
convertTime(){


  var currentTime = DateTime.now().millisecondsSinceEpoch +1000 *widget.balTime!;

setState(() {
  countertime= currentTime;
});


}
onEnd(){
 setState(() {
   isEnded= true;
 });
}
@override
void initState(){
  
  convertTime();
  
super.initState();
}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
          Card(
                  elevation: 5.0,
                child: Container(
                  
                  height: MediaQuery.of(context).size.height/6,
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top:18.0,left:18.0),
                            child: Column(
                              children: [
                                Container(
                                                height:
                                                    MediaQuery.of(context).size.height *0.08,
                                                width:
                                                    MediaQuery.of(context).size.width*0.15,
                                                decoration: BoxDecoration(
                                                  
                                                  borderRadius: BorderRadius.all(
                                                      Radius.circular(5)),
                                                  image: DecorationImage(
                                                    image: AssetImage('assets/mcc.png'),
                                                    fit: BoxFit.fill,
                                                  ),
                                                ),
                                              ),
                                              SizedBox(height: 5.0,),
                                              Text(
                                              widget.team1!,
                                              style: TextStyle(
                                                  
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 15),
                                            ),
                                          
                                                                                 Text(
                                              "Start Date " +widget.startDate!.toString() ,
                                              style: TextStyle(
                                                  
                                                  //fontWeight: FontWeight.bold,
                                                  fontSize: 10),
                                            ),
                                           
                              ],
                            ),
                          ),
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Text(
                                              "Vs",
                                              style: TextStyle(
                                                 
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 25),
                                            ),
                                             CountdownTimer(
                                              controller: controller,
  endTime: countertime,
  onEnd: onEnd,
),
                                          ],
                                        ),
                                         Padding(
                                           padding: const EdgeInsets.only(top:18.0, right:18.0),
                                           child: Column(
                                             children: [
                                               Container(
                                               height:
                                                    MediaQuery.of(context).size.height *0.08,
                                                width:
                                                    MediaQuery.of(context).size.width*0.15,
                                                decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.all(
                                                      Radius.circular(5)),
                                                  image: DecorationImage(
                                                    image: AssetImage('assets/mufc.png'),
                                                    fit: BoxFit.fill,
                                                  ),
                                                ),
                                        ),
                                         Text(
                                              widget.team2!,
                                              style: TextStyle(
                                                  
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 15),
                                            ),
                                      Text(
                                              "End Date " +widget.startTime!.toString() ,
                                                 style: TextStyle(
                                                  
                                                  //fontWeight: FontWeight.bold,
                                                  fontSize: 10),
                                      )
                                                

                                            
                                             ],
                                           ),
                                         ),
                        ],
                      ),
                                    
                    ],
                  ),
                ),
                 
                ),
            
            SizedBox(
              height: 10,
            ),
           
            Center(
                child: Text(
              "Select Contest that you wish to play",
              style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            )),
            SizedBox(
              height: 10,
            ),
            
            ListView.builder(
              shrinkWrap: true,
              itemCount: widget.priceDetails!.length,
              itemBuilder:(context, index){
                return InkWell(
                  onTap: () async{
                    if(isEnded == false){
 Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => QuestionPage(
eventId:widget.priceDetails![index]["event_id"],
pricePoolId:widget.priceDetails![index]["id"],
duration:countertime ,
    )));
                    }else{
                      Fluttertoast.showToast(msg: "Event Ended ");
                    }
                                      
  
                  },
                                  child: Container(
              height: MediaQuery.of(context).size.height * 0.150,
              width: MediaQuery.of(context).size.width * 0.995,
              child: Card(
                  color: Colors.blueGrey[500],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  clipBehavior: Clip.antiAlias,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Entry Fee ${widget.priceDetails![index]["entry_fee"]}",
                          style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.bold,
                              fontSize: 12),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Prize Money",
                              style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 10),
                            ),
                            Text(
                              "Contestent",
                              style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 10),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(widget.priceDetails![index]["prize_money"].toString()
                              ,
                              style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 10),
                            ),
                            Text(
                              widget.priceDetails![index]["contestent"].toString(),
                              style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 10),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
              ),
            ),
                );
              })
          ],
        ),
      ),
    );
  }
}
