import 'package:flutter/material.dart';
import 'package:sporket/pages/football_details.dart';

class FootballPage extends StatelessWidget {
  const FootballPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.280,
              width: MediaQuery.of(context).size.width * 0.995,
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FootballDetailsPage()));
                },
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  clipBehavior: Clip.antiAlias,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              height:
                                  MediaQuery.of(context).size.height * 0.120,
                              width: MediaQuery.of(context).size.height * 0.10,
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5)),
                                image: DecorationImage(
                                  image: AssetImage('assets/i2.jpg'),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            Container(
                              height:
                                  MediaQuery.of(context).size.height * 0.145,
                              width: MediaQuery.of(context).size.height * 0.350,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(1)),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    // SizedBox(
                                    //   height: 30,
                                    // ),
                                    Text(
                                      "FIFA",
                                      style: TextStyle(
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.bold,
                                          fontSize: 22),
                                    ),
                                    SizedBox(
                                      height: 35,
                                    ),
                                    Text(
                                      "Regular Championship ",
                                      style: TextStyle(
                                          fontFamily: 'Montserrat',
                                          //fontWeight: FontWeight.bold,
                                          fontSize: 10),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        const Divider(
                          color: Colors.black,
                        ),
                        Row(
                          //mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              //crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Popularity"),
                                //Text("Popularity"),
                                SizedBox(
                                  height: MediaQuery.of(context).size.height *
                                      0.004,
                                  width:
                                      MediaQuery.of(context).size.width * 0.1,
                                  child: LinearProgressIndicator(
                                      //value: animation.value,
                                      backgroundColor: Colors.white
                                      //valueColor: changeColor),
                                      ),
                                ),
                              ],
                            ),
                            SizedBox(
                              width: 150,
                            ),
                            Column(
                              children: [
                                Text("Overall rating"),
                                //Text("Popularity"),
                                SizedBox(
                                  height: MediaQuery.of(context).size.height *
                                      0.004,
                                  width:
                                      MediaQuery.of(context).size.width * 0.1,
                                  child: LinearProgressIndicator(
                                      //value: animation.value,
                                      color: Colors.white,
                                      backgroundColor: Colors.orange
                                      //valueColor: changeColor),
                                      ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.280,
              width: MediaQuery.of(context).size.width * 0.995,
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FootballDetailsPage()));
                },
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  clipBehavior: Clip.antiAlias,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              height:
                                  MediaQuery.of(context).size.height * 0.120,
                              width: MediaQuery.of(context).size.height * 0.10,
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5)),
                                image: DecorationImage(
                                  image: AssetImage('assets/i2.jpg'),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            Container(
                              height:
                                  MediaQuery.of(context).size.height * 0.145,
                              width: MediaQuery.of(context).size.height * 0.350,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(1)),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    // SizedBox(
                                    //   height: 30,
                                    // ),
                                    Text(
                                      "FIFA ",
                                      style: TextStyle(
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.bold,
                                          fontSize: 22),
                                    ),
                                    SizedBox(
                                      height: 35,
                                    ),
                                    Text(
                                      "Regular Championship ",
                                      style: TextStyle(
                                          fontFamily: 'Montserrat',
                                          //fontWeight: FontWeight.bold,
                                          fontSize: 10),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        const Divider(
                          color: Colors.black,
                        ),
                        Row(
                          //mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              //crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Popularity"),
                                //Text("Popularity"),
                                SizedBox(
                                  height: MediaQuery.of(context).size.height *
                                      0.004,
                                  width:
                                      MediaQuery.of(context).size.width * 0.1,
                                  child: LinearProgressIndicator(
                                      //value: animation.value,
                                      backgroundColor: Colors.white
                                      //valueColor: changeColor),
                                      ),
                                ),
                              ],
                            ),
                            SizedBox(
                              width: 150,
                            ),
                            Column(
                              children: [
                                Text("Overall rating"),
                                //Text("Popularity"),
                                SizedBox(
                                  height: MediaQuery.of(context).size.height *
                                      0.004,
                                  width:
                                      MediaQuery.of(context).size.width * 0.1,
                                  child: LinearProgressIndicator(
                                      //value: animation.value,
                                      color: Colors.white,
                                      backgroundColor: Colors.orange
                                      //valueColor: changeColor),
                                      ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.280,
              width: MediaQuery.of(context).size.width * 0.995,
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FootballDetailsPage()));
                },
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  clipBehavior: Clip.antiAlias,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              height:
                                  MediaQuery.of(context).size.height * 0.120,
                              width: MediaQuery.of(context).size.height * 0.10,
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5)),
                                image: DecorationImage(
                                  image: AssetImage('assets/i2.jpg'),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            Container(
                              height:
                                  MediaQuery.of(context).size.height * 0.145,
                              width: MediaQuery.of(context).size.height * 0.350,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(1)),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    // SizedBox(
                                    //   height: 30,
                                    // ),
                                    Text(
                                      "FIFA",
                                      style: TextStyle(
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.bold,
                                          fontSize: 22),
                                    ),
                                    SizedBox(
                                      height: 35,
                                    ),
                                    Text(
                                      "Regular Championship ",
                                      style: TextStyle(
                                          fontFamily: 'Montserrat',
                                          //fontWeight: FontWeight.bold,
                                          fontSize: 10),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        const Divider(
                          color: Colors.black,
                        ),
                        Row(
                          //mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              //crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Popularity"),
                                //Text("Popularity"),
                                SizedBox(
                                  height: MediaQuery.of(context).size.height *
                                      0.004,
                                  width:
                                      MediaQuery.of(context).size.width * 0.1,
                                  child: LinearProgressIndicator(
                                      //value: animation.value,
                                      backgroundColor: Colors.white
                                      //valueColor: changeColor),
                                      ),
                                ),
                              ],
                            ),
                            SizedBox(
                              width: 150,
                            ),
                            Column(
                              children: [
                                Text("Overall rating"),
                                //Text("Popularity"),
                                SizedBox(
                                  height: MediaQuery.of(context).size.height *
                                      0.004,
                                  width:
                                      MediaQuery.of(context).size.width * 0.1,
                                  child: LinearProgressIndicator(
                                      //value: animation.value,
                                      color: Colors.white,
                                      backgroundColor: Colors.orange
                                      //valueColor: changeColor),
                                      ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.280,
              width: MediaQuery.of(context).size.width * 0.995,
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FootballDetailsPage()));
                },
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  clipBehavior: Clip.antiAlias,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              height:
                                  MediaQuery.of(context).size.height * 0.120,
                              width: MediaQuery.of(context).size.height * 0.10,
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5)),
                                image: DecorationImage(
                                  image: AssetImage('assets/i2.jpg'),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            Container(
                              height:
                                  MediaQuery.of(context).size.height * 0.145,
                              width: MediaQuery.of(context).size.height * 0.350,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(1)),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    // SizedBox(
                                    //   height: 30,
                                    // ),
                                    Text(
                                      "FIFA ",
                                      style: TextStyle(
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.bold,
                                          fontSize: 22),
                                    ),
                                    SizedBox(
                                      height: 35,
                                    ),
                                    Text(
                                      "Regular Championship ",
                                      style: TextStyle(
                                          fontFamily: 'Montserrat',
                                          //fontWeight: FontWeight.bold,
                                          fontSize: 10),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        const Divider(
                          color: Colors.black,
                        ),
                        Row(
                          //mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              //crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Popularity"),
                                //Text("Popularity"),
                                SizedBox(
                                  height: MediaQuery.of(context).size.height *
                                      0.004,
                                  width:
                                      MediaQuery.of(context).size.width * 0.1,
                                  child: LinearProgressIndicator(
                                      //value: animation.value,
                                      backgroundColor: Colors.white
                                      //valueColor: changeColor),
                                      ),
                                ),
                              ],
                            ),
                            SizedBox(
                              width: 150,
                            ),
                            Column(
                              children: [
                                Text("Overall rating"),
                                //Text("Popularity"),
                                SizedBox(
                                  height: MediaQuery.of(context).size.height *
                                      0.004,
                                  width:
                                      MediaQuery.of(context).size.width * 0.1,
                                  child: LinearProgressIndicator(
                                      //value: animation.value,
                                      color: Colors.white,
                                      backgroundColor: Colors.orange
                                      //valueColor: changeColor),
                                      ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
