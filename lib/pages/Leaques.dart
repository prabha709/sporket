import 'package:flutter/material.dart';
import 'package:sporket/pages/contest_page_details.dart';

class Leaques extends StatefulWidget {
  final String ?title;
  final List ?leaques;

  Leaques({
    this.leaques,
    this.title
  });
  @override
  _LeaquesState createState() => _LeaquesState();
}

class _LeaquesState extends State<Leaques> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(widget.title!,style: TextStyle(color: Colors.black)),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: ListView.builder(
        itemCount: widget.leaques!.length,
        itemBuilder: (context, index){
          return Padding(
            padding: const EdgeInsets.all(18.0),
            child: InkWell(
              onTap: (){
                 Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ContestPageDetais(
                                    startDate: widget.leaques![index]
                                        ["end_time"],
                                    startTime: widget.leaques![index]
                                        ["start_time"],
                                    team1: widget.leaques![index]
                                        ["team1"],
                                    team2: widget.leaques![index]
                                        ["team2"],
                                    priceDetails: widget.leaques![index]
                                        ["prizepools"],
                                        balTime: widget.leaques![index]
                                        ["balance_time"],
                                  )));
              },
                          child: Card(
                elevation: 5.0,
                child: Container(
                   height: MediaQuery.of(context).size.height/5,
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top:18.0,left:18.0),
                                child: Column(
                                  children: [
                                    Container(
                                                    height:
                                                        MediaQuery.of(context).size.height *0.08,
                                                    width:
                                                        MediaQuery.of(context).size.width*0.15,
                                                    decoration: BoxDecoration(
                                                      
                                                      borderRadius: BorderRadius.all(
                                                          Radius.circular(5)),
                                                      image: DecorationImage(
                                                        image: AssetImage('assets/mcc.png'),
                                                        fit: BoxFit.fill,
                                                      ),
                                                    ),
                                                  ),
                                                  SizedBox(height: 5.0,),
                                                  Text(
                                                   widget.leaques![index]["team1"],
                                                  style: TextStyle(
                                                      
                                                      fontWeight: FontWeight.bold,
                                                      fontSize: 15),
                                                ),
                                                  Text(
                                                  "Start Date " + widget.leaques![index]["start_time"],
                                                  style: TextStyle(
                                                      
                                                      //fontWeight: FontWeight.bold,
                                                      fontSize: 10),
                                                ),
                                               
                                  ],
                                ),
                              ),
                                            Text(
                                              "Vs",
                                              style: TextStyle(
                                                 
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 25),
                                            ),
                                             Padding(
                                               padding: const EdgeInsets.only(top:18.0, right:18.0),
                                               child: Column(
                                                 children: [
                                                   Container(
                                                   height:
                                                        MediaQuery.of(context).size.height *0.08,
                                                    width:
                                                        MediaQuery.of(context).size.width*0.15,
                                                    decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.all(
                                                          Radius.circular(5)),
                                                      image: DecorationImage(
                                                        image: AssetImage('assets/mufc.png'),
                                                        fit: BoxFit.fill,
                                                      ),
                                                    ),
                                            ),
                                            Text(
                                                   widget.leaques![index]["team2"],
                                                  style: TextStyle(
                                                      
                                                      fontWeight: FontWeight.bold,
                                                      fontSize: 15),
                                                ),
                                            
                                              Text(
                                                 "End Date " + widget.leaques![index]["end_time"],
                                                  style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      //fontWeight: FontWeight.bold,
                                                      fontSize: 10),
                                                ),
                                                
                                                 ],
                                               ),
                                             ),
                            ],
                          ),
                                        
                        ],
                      ),
                ),

              ),
            ),
          );
        }),
      
    );
  }
}