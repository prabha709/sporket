import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart'as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'package:sporket/Utils/BaseUrl.dart';
import 'dart:convert';
class QuestionPage extends StatefulWidget {
  final String ?eventId;
  final String ?pricePoolId;
  final int? duration;

  QuestionPage({
    this.eventId,
    this.pricePoolId,
    this.duration
  });
  @override
  _QuestionPageState createState() => _QuestionPageState();
}

class _QuestionPageState extends State<QuestionPage> {
  bool loading = false;
Map ?data;
joinContest() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
   String? token = prefs.getString("token");
  String url =BaseUrl.url+"events/joinContest/${widget.pricePoolId}/";
  var response = await http.post(Uri.parse("$url"),headers: {"Authorization":"Bearer $token"});
  if(response.statusCode == 200){
    var jsonObject= json.decode(response.body);
    setState(() {
      data=jsonObject;
    });
    return jsonObject;
  }else{
    return null;
  }
}
@override
  void initState() {
   joinContest();
    super.initState();
  }
getQuestions()async{
  print(widget.eventId);
  print(widget.pricePoolId);
    SharedPreferences prefs = await SharedPreferences.getInstance();
   String? token = prefs.getString("token");
  String url =BaseUrl.url+"events/prizepool/${widget.eventId}/${widget.pricePoolId}/";
  var response = await http.get(Uri.parse("$url"),headers: {"Authorization":"Bearer $token"});
  if(response.statusCode == 200){
    var jsonObject= json.decode(response.body);
   
    return jsonObject["data"];
  }else{
    return null;
  }
}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
backgroundColor: Colors.white,
iconTheme: IconThemeData(
  color: Colors.black
),
      ),
      body: loading == false?FutureBuilder(
        future: getQuestions(),
        builder: (context, snapshot){
          if(snapshot.connectionState == ConnectionState.done){
            if(snapshot.data == null){
              return Center(
                child: Text("No questions are available in this event"),
              );
            }else{
              final questions= snapshot.data as Map;
              return Column(
                children: [
    
                  Card(
                    elevation: 1.0,
                                  child: ExpansionTile(
                                    initiallyExpanded: true,
                      title:Text(questions["question_text"]),
                    children: [
                      ListView.builder(
                        shrinkWrap: true,
                        itemCount:questions["options"].length,
                        itemBuilder: (context, index){
                          return InkWell(
                            onTap: (){
                              showAlertDialog(context,questions["options"][index]["option_text"],questions["options"][index]["bid_coin"],questions["id"].toString());
                            },
                                                      child: ListTile(
                              title: Text(questions["options"][index]["option_text"]),
                              trailing: Container(
                              
                                width: 109,
                                height: 23.0,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                color: Colors.grey,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Image.asset("assets/gcoinx.png"),
                                    Text((questions["options"][index]["bid_coin"].toString())),
                                    Container(
                                      width: 30.0,
                                      height: 23.0,
                                     decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(topRight: Radius.circular(10.0),bottomRight:Radius.circular(10.0) ),
                                color: Color(0xffC11F23),
                                
                                ),
                                child: Center(child: Text("Bid",style: TextStyle(color: Colors.white),)),
                                    )
                                  ],
                                ),
                                
                              ),
                            ),
                          );
                        },
                      )
                    ],
                     ),
                  ),
                ],
              );
            }
          }else{
            return Center(child: CircularProgressIndicator(),);
          }
        },
      ):Center(child: CircularProgressIndicator(),),
      
    );
  }
}
showAlertDialog(BuildContext context, String title, int amount, String questionId) {  
  // Create button  
  Widget okButton = TextButton(  
    child: Text("yes"),  
    onPressed: () async{ 
    
        SharedPreferences prefs = await SharedPreferences.getInstance();
   String? token = prefs.getString("token");
  String url =BaseUrl.url+"events/userSubmitAnswer/$questionId/";
  Map data={
"user_answer": [
    "$title", amount
  ]
  };
  var response = await http.patch(Uri.parse("$url"),headers: {"Authorization":"Bearer $token","Content-Type": "application/json"},
  body: json.encode(data)
  );
  if(response.statusCode == 200){
    Fluttertoast.showToast(msg: "You bitted SuccesFully");
   Navigator.of(context).pop(); 
  }else if(response.statusCode == 400){
   
     Fluttertoast.showToast(msg: "You have already Bitted for this Question");
     Navigator.of(context).pop(); 
  }
    else{
      
      Navigator.of(context).pop(); 
    return null;
  } 
       
    },  
  );  
  Widget cancelButton = TextButton(  
    child: Text("no"),  
    onPressed: () {  
      Navigator.of(context).pop();  
    },  
  );  
  
  // Create AlertDialog  
  AlertDialog alert = AlertDialog(  
    title: Text("Bitting on  $title"),  
    content: Text("Are you Sure want to bit $amount?."),  
    actions: [  
      okButton, 
      cancelButton 
    ],  
  );  
  
  // show the dialog  
  showDialog(  
    context: context,  
    builder: (BuildContext context) {  
      return alert;  
    },  
  );  
} 