import 'package:flutter/material.dart';
import 'package:sporket/widgtes/nav_drawer.dart';

class FootballDetailsPage extends StatefulWidget {
  const FootballDetailsPage({Key? key}) : super(key: key);

  @override
  _FootballDetailsPageState createState() => _FootballDetailsPageState();
}

class _FootballDetailsPageState extends State<FootballDetailsPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          foregroundColor: Colors.black,
          leading: BackButton(color: Colors.black),
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {},
                  child: Icon(
                    Icons.account_balance_wallet,
                    color: Colors.black,
                    size: 26.0,
                  ),
                )),
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {},
                  child: Icon(
                    Icons.notifications,
                    color: Colors.black,
                  ),
                )),
          ]),
      drawer: NavDrawer(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.280,
              width: MediaQuery.of(context).size.width * 0.995,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
                clipBehavior: Clip.antiAlias,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            height: MediaQuery.of(context).size.height * 0.120,
                            width: MediaQuery.of(context).size.height * 0.10,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              image: DecorationImage(
                                image: AssetImage('assets/i2.jpg'),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height * 0.145,
                            width: MediaQuery.of(context).size.height * 0.350,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(1)),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  // SizedBox(
                                  //   height: 30,
                                  // ),
                                  Text(
                                    "FIFA",
                                    style: TextStyle(
                                        fontFamily: 'Montserrat',
                                        fontWeight: FontWeight.bold,
                                        fontSize: 22),
                                  ),
                                  SizedBox(
                                    height: 35,
                                  ),
                                  Text(
                                    "Regular Championship ",
                                    style: TextStyle(
                                        fontFamily: 'Montserrat',
                                        //fontWeight: FontWeight.bold,
                                        fontSize: 10),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      const Divider(
                        color: Colors.black,
                      ),
                      Row(
                        //mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Column(
                            //crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Popularity"),
                              //Text("Popularity"),
                              SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.004,
                                width: MediaQuery.of(context).size.width * 0.1,
                                child: LinearProgressIndicator(
                                    //value: animation.value,
                                    backgroundColor: Colors.white
                                    //valueColor: changeColor),
                                    ),
                              ),
                            ],
                          ),
                          SizedBox(
                            width: 150,
                          ),
                          Column(
                            children: [
                              Text("Overall rating"),
                              //Text("Popularity"),
                              SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.004,
                                width: MediaQuery.of(context).size.width * 0.1,
                                child: LinearProgressIndicator(
                                    //value: animation.value,
                                    color: Colors.white,
                                    backgroundColor: Colors.orange
                                    //valueColor: changeColor),
                                    ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(24.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Upcoming"),
                  Text("Finished"),
                ],
              ),
            ),
            Card(
              //color: Colors.yellow,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.145,
                          width: MediaQuery.of(context).size.height * 0.455,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(1)),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.050,
                                      width:
                                          MediaQuery.of(context).size.height *
                                              0.050,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5)),
                                        image: DecorationImage(
                                          image: AssetImage('assets/mcc.png'),
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    ),
                                    // SizedBox(
                                    //   width: 65,
                                    // ),
                                    Text(
                                      "Vs",
                                      style: TextStyle(
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14),
                                    ),
                                    // SizedBox(
                                    //   width: 65,
                                    // ),
                                    Container(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.050,
                                      width:
                                          MediaQuery.of(context).size.height *
                                              0.050,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5)),
                                        image: DecorationImage(
                                          image: AssetImage('assets/mufc.png'),
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 1,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Manchester City",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          "Start Date 01 May 2021",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              //fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          "Start Time",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              //fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      width: 1,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Text(
                                          "Manchester United",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          "Timer 01 May 2021",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              //fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          "",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              //fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 3,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    // Row(
                    //   children: [
                    //     Column(
                    //       children: [
                    //         Text("Start Date 01 May 2021"),
                    //         Text("Start Time")
                    //       ],
                    //     ),
                    //     Text("Timer 01 May")
                    //   ],
                    // )
                  ],
                ),
              ),
            ),
            Card(
              //color: Colors.yellow,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.145,
                          width: MediaQuery.of(context).size.height * 0.455,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(1)),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.050,
                                      width:
                                          MediaQuery.of(context).size.height *
                                              0.050,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5)),
                                        image: DecorationImage(
                                          image: AssetImage('assets/mcc.png'),
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    ),
                                    // SizedBox(
                                    //   width: 65,
                                    // ),
                                    Text(
                                      "Vs",
                                      style: TextStyle(
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14),
                                    ),
                                    // SizedBox(
                                    //   width: 65,
                                    // ),
                                    Container(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.050,
                                      width:
                                          MediaQuery.of(context).size.height *
                                              0.050,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5)),
                                        image: DecorationImage(
                                          image: AssetImage('assets/mufc.png'),
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 1,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Manchester City",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          "Start Date 01 May 2021",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              //fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          "Start Time",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              //fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      width: 1,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Text(
                                          "Manchester United",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          "Timer 01 May 2021",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              //fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          "",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              //fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 3,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    // Row(
                    //   children: [
                    //     Column(
                    //       children: [
                    //         Text("Start Date 01 May 2021"),
                    //         Text("Start Time")
                    //       ],
                    //     ),
                    //     Text("Timer 01 May")
                    //   ],
                    // )
                  ],
                ),
              ),
            ),
            Card(
              //color: Colors.yellow,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.145,
                          width: MediaQuery.of(context).size.height * 0.455,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(1)),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.050,
                                      width:
                                          MediaQuery.of(context).size.height *
                                              0.050,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5)),
                                        image: DecorationImage(
                                          image: AssetImage('assets/mcc.png'),
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    ),
                                    // SizedBox(
                                    //   width: 65,
                                    // ),
                                    Text(
                                      "Vs",
                                      style: TextStyle(
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14),
                                    ),
                                    // SizedBox(
                                    //   width: 65,
                                    // ),
                                    Container(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.050,
                                      width:
                                          MediaQuery.of(context).size.height *
                                              0.050,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5)),
                                        image: DecorationImage(
                                          image: AssetImage('assets/mufc.png'),
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 1,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Manchester City",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          "Start Date 01 May 2021",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              //fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          "Start Time",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              //fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      width: 1,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Text(
                                          "Manchester United",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          "Timer 01 May 2021",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              //fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          "",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              //fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 3,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    // Row(
                    //   children: [
                    //     Column(
                    //       children: [
                    //         Text("Start Date 01 May 2021"),
                    //         Text("Start Time")
                    //       ],
                    //     ),
                    //     Text("Timer 01 May")
                    //   ],
                    // )
                  ],
                ),
              ),
            ),
            Card(
              //color: Colors.yellow,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.145,
                          width: MediaQuery.of(context).size.height * 0.455,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(1)),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.050,
                                      width:
                                          MediaQuery.of(context).size.height *
                                              0.050,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5)),
                                        image: DecorationImage(
                                          image: AssetImage('assets/mcc.png'),
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    ),
                                    // SizedBox(
                                    //   width: 65,
                                    // ),
                                    Text(
                                      "Vs",
                                      style: TextStyle(
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14),
                                    ),
                                    // SizedBox(
                                    //   width: 65,
                                    // ),
                                    Container(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.050,
                                      width:
                                          MediaQuery.of(context).size.height *
                                              0.050,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5)),
                                        image: DecorationImage(
                                          image: AssetImage('assets/mufc.png'),
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 1,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Manchester City",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          "Start Date 01 May 2021",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              //fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          "Start Time",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              //fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      width: 1,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Text(
                                          "Manchester United",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          "Timer 01 May 2021",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              //fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          "",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              //fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 3,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    // Row(
                    //   children: [
                    //     Column(
                    //       children: [
                    //         Text("Start Date 01 May 2021"),
                    //         Text("Start Time")
                    //       ],
                    //     ),
                    //     Text("Timer 01 May")
                    //   ],
                    // )
                  ],
                ),
              ),
            ),
            Card(
              //color: Colors.yellow,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.145,
                          width: MediaQuery.of(context).size.height * 0.455,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(1)),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.050,
                                      width:
                                          MediaQuery.of(context).size.height *
                                              0.050,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5)),
                                        image: DecorationImage(
                                          image: AssetImage('assets/mcc.png'),
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    ),
                                    // SizedBox(
                                    //   width: 65,
                                    // ),
                                    Text(
                                      "Vs",
                                      style: TextStyle(
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14),
                                    ),
                                    // SizedBox(
                                    //   width: 65,
                                    // ),
                                    Container(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.050,
                                      width:
                                          MediaQuery.of(context).size.height *
                                              0.050,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5)),
                                        image: DecorationImage(
                                          image: AssetImage('assets/mufc.png'),
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 1,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Manchester City",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          "Start Date 01 May 2021",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              //fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          "Start Time",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              //fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      width: 1,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Text(
                                          "Manchester United",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          "Timer 01 May 2021",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              //fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          "",
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              //fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 3,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    // Row(
                    //   children: [
                    //     Column(
                    //       children: [
                    //         Text("Start Date 01 May 2021"),
                    //         Text("Start Time")
                    //       ],
                    //     ),
                    //     Text("Timer 01 May")
                    //   ],
                    // )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
