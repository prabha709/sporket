import 'package:flutter/material.dart';

class CommentsTabPage extends StatelessWidget {
  const CommentsTabPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          // SizedBox(
          //   height: 400,
          // ),
          Row(
            children: <Widget>[
              Material(
                child: new Container(
                  margin: new EdgeInsets.symmetric(horizontal: 1.0),
                  child: new IconButton(
                    onPressed: () {},
                    icon: new Icon(Icons.face),
                    color: Colors.teal,
                  ),
                ),
                color: Colors.white,
              ),

              // Text input
              Flexible(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.050,
                  width: MediaQuery.of(context).size.width * 0.900,
                  child: TextField(
                    style:
                        TextStyle(color: Colors.blueGrey[500], fontSize: 15.0),
                    //controller: textEditingController,
                    decoration: InputDecoration.collapsed(
                      hintText: 'Type a message',
                      hintStyle: TextStyle(
                        color: Colors.teal,
                      ),
                    ),
                  ),
                ),
              ),

              // Send Message Button
              Material(
                child: new Container(
                  margin: new EdgeInsets.symmetric(horizontal: 8.0),
                  child: new IconButton(
                    icon: new Icon(Icons.send),
                    onPressed: () => {},
                    color: Colors.teal,
                  ),
                ),
                color: Colors.white,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
