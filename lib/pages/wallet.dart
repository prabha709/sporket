import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sporket/Utils/BaseUrl.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:sporket/pages/notification_page.dart';

class Wallet extends StatefulWidget {
  @override
  _WalletState createState() => _WalletState();
}

class _WalletState extends State<Wallet> {
  String? amount;
  static const platform = const MethodChannel("razorpay_flutter");

  late Razorpay _razorpay;
  @override
  void initState() {
    super.initState();
    getWallet();
    _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  walletUpdate(String paymentId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString("token");
    String url = BaseUrl.url + "events/wallet/";
    Map data = {
      "amount": 500,
      "status": "Success",
      "transaction_id": "$paymentId"
    };

    var response = await http.post(Uri.parse("$url"),
        headers: {
          "Authorization": "Bearer $token",
          "Content-Type": "application/json"
        },
        body: json.encode(data));
    if (response.statusCode == 200) {
      var jsonObject = json.decode(response.body);
      getWallet();
    } else {
      FailureMessage(message: "Money Not added to wallet").failedMessage();
      return null;
    }
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    walletUpdate(response.paymentId!.toString());

    Fluttertoast.showToast(
        msg: "SUCCESS: " + response.paymentId!,
        toastLength: Toast.LENGTH_SHORT);
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    Fluttertoast.showToast(
        msg: "ERROR: " + response.code.toString() + " - " + response.message!,
        toastLength: Toast.LENGTH_SHORT);
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    Fluttertoast.showToast(
        msg: "EXTERNAL_WALLET: " + response.walletName!,
        toastLength: Toast.LENGTH_SHORT);
  }

  @override
  void dispose() {
    super.dispose();
    _razorpay.clear();
  }

  void openCheckout() async {
    var options = {
      'key': 'rzp_test_1DP5mmOlF5G5ag',
      'amount': 500,
      'name': 'Acme Corp.',
      'description': 'Fine T-Shirt',
      'prefill': {'contact': '7305822599', 'email': 'test@razorpay.com'},
      'external': {
        'wallets': ['paytm']
      }
    };

    try {
      _razorpay.open(options);
    } catch (e) {
      debugPrint('Error: e');
    }
  }

  getWallet() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString("token");
    String url = BaseUrl.url + "events/wallet/";
    var response = await http
        .get(Uri.parse("$url"), headers: {"Authorization": "Bearer $token"});
    if (response.statusCode == 200) {
      var jsonObject = json.decode(response.body);
      setState(() {
        amount = jsonObject["data"]["wallet_points"].toString();
      });
      return jsonObject["data"]["wallet_points"];
    } else {
      FailureMessage(message: "Internal Servver Error").failedMessage();
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            foregroundColor: Colors.black,
            leading: BackButton(
              color: Colors.black,
            ),
            actions: <Widget>[
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => NotificationPage()));
                    },
                    child: Icon(
                      Icons.notifications,
                      color: Colors.black,
                    ),
                  )),
            ]),
        body: SafeArea(
          child: Container(
            width: MediaQuery.of(context).size.width / 0.9,
            height: MediaQuery.of(context).size.height / 3.8,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  ListTile(
                    title: Text('My Wallet balance',
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        )),
                    subtitle: Text(
                      "Play an win more cash",
                      style: TextStyle(
                          fontFamily: 'Montserrat',
                          color: Colors.white,
                          fontSize: 12),
                    ),
                    trailing: CircleAvatar(
                      backgroundColor: Colors.white,
                      radius: 39,
                      child: amount != null
                          ? Text(
                              '\$ ' + amount!,
                              style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold),
                            )
                          : Center(child: CircularProgressIndicator()), //Text
                    ),
                  ),
                  ListTile(
                    title: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Text('Enter Amount',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Montserrat',
                            fontSize: 12,
                          )),
                    ),
                    subtitle: Stack(children: [
                      Container(
                        height: 30,
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          textAlign: TextAlign.left,
                          cursorHeight: 18,
                          style: TextStyle(
                              backgroundColor: Colors.white,
                              color: Colors.black,
                              fontSize: 15,
                              fontWeight: FontWeight.bold),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.fromLTRB(30, 14, 0, 0),
                            focusedBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.0)),
                              borderSide: BorderSide(color: Colors.white),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.0)),
                              borderSide:
                                  BorderSide(color: Colors.white, width: 2),
                            ),
                            hintStyle: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 14,
                            ),
                            filled: true,
                            fillColor: Colors.white,
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)),
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Text(
                              "₹",
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              color: Color(0xff0A37E0),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.0)),
                            ),
                            height: MediaQuery.of(context).size.height * 0.04,
                            width: MediaQuery.of(context).size.width * 0.35,
                            child: MaterialButton(
                              onPressed: () {
                                openCheckout();
                              },
                              child: Text(
                                'Add Money',
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 9,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ]),
                  ),
                ],
              ),
            ),
            alignment: Alignment.topCenter,
            margin: EdgeInsets.fromLTRB(20, 100, 20, 100),
            decoration: BoxDecoration(
              color: HexColor("#C11F23"),
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20),
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20)),
            ),
          ),
        ));
  }
}
